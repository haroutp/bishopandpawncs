﻿using System;

namespace BishopAndPawn
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static bool bishopAndPawn(string bishop, string pawn) {
            return bishop[0] + bishop[1] == pawn[0] + pawn[1] || bishop[0] + pawn[1] == pawn[0] + bishop[1];
        }

    }
}
